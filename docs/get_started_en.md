# Prerequisites

- numpy 1.17+
- opencv-python 4.1+
- pytest 4.3+
- [mindspore](https://www.mindspore.cn/install) 1.2+

# Installation

## Prepare environment

- Create a conda virtual environment and activate it.

```shell
conda create -n mindvision python=3.7.5 -y
conda activate mindvision
```

- Install MindSpore

```shell
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.3.0/MindSpore/ascend/aarch64/mindspore_ascend-1.3.0-cp37-cp37m-linux_aarch64.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
```

## Install MindVision

- Clone the MindVsion repository.

```shell
git clone https://gitee.com/mindspore/mindvision.git
cd mindvision
```

- Install

```python
python setup.py install
```

## Verification

To verify whether MindVision and the required environment are installed correctly, we can run sample Python code to initialize a classificer and run inference a demo image:

```shell
update
```

The above code is supposed to run successfully upon you finish the installation.
