# mindvision-pose

## Introduction

MindVision-Pose is an open-source toolbox for pose estimation based on MindSpore. It is a part of the MindVision project.

The master branch develop on **MindSPore 1.2**.

![demo image](docs/demo1.png)

## License

This project is released under the [Apache 2.0 license](LICENSE).

## Benchmark and model zoo

Results and models are available in the [model zoo](docs/model_zoo.md).

Supporting backbones:

- [x] [AlexNet]
- [x] [CPM]
- [x] [Hourglass]
- [x] [HRNet]
- [x] [MobilenetV2]
- [x] [ResNet]
- [x] [ResNetV1D]
- [x] [ResNext]
- [x] [SCNet]
- [x] [SEResNet]
- [x] [ShufflenetV1]

Supported methods for human pose estimation:

- [x] [CPM]
- [x] [SimpleBaseline]
- [x] [HRNet]
- [x] [Hourglass]
- [x] [SCNet]
- [x] [HigherHRNet]
- [x] [DarkPose]

Supported datasets:

- [x] [COCO](http://cocodataset.org/)
- [x] [MPII](http://human-pose.mpi-inf.mpg.de/)
- [x] [MPII-TRB](https://github.com/kennymckormick/Triplet-Representation-of-human-Body)
- [x] [AI Challenger](https://github.com/AIChallenger/AI_Challenger_2017)
- [x] [OCHuman](https://github.com/liruilong940607/OCHumanApi)
- [x] [CrowdPose](https://github.com/Jeff-sjtu/CrowdPose)
- [x] [OneHand10K](https://www.yangangwang.com/papers/WANG-MCC-2018-10.html)

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mindspore/mindvision/issues).

## Contributing

We appreciate all contributions to improve MindSpore Segmentation. Please refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for the contributing guideline.

## Contributors

## Acknowledgement

MindVision is an open source project that welcome any contribution and feedback. We wish that the toolbox and benchmark could serve the growing research community by providing a flexible as well as standardized toolkit to reimplement existing methods and develop their own new pose estimation methods.
