# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""register dataset"""
from dataset.utils.coco import COCOYoloDataset
from dataset.utils.sampler import DistributedSampler
from dataset.utils.mindrecord import Coco2MindRecord

from dataset.pipelines.loading import LoadMindDataset
from dataset.pipelines.builtin_transforms import *
from dataset.pipelines.transforms import *

from dataset.builder import register_ms_builtin_dataset

register_ms_builtin_dataset()
