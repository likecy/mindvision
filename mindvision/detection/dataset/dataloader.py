# -*- coding: utf-8 -*-

# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""the module is used to load transforms pipeline of dataset."""

import cv2

import mindspore.dataset as de
from dataset.builder import build_transforms, build_dataset_sampler, \
    build_dataset


class DataLoader:
    """The dataset loader class.

    Args:
        dataset: dataset object
        map_cfg (Config) : Config for ds.map().
        batch_cfg (Config) : Config for ds.batch().
        config (Config) : The data loader config dict.

    Examples:
        >>> dataloader = DataLoader(ds, map, batch, cfg)
        >>> ds = dataloder()
    """

    def __init__(self,
                 dataset,
                 map_cfg=None,
                 batch_cfg=None,
                 config=None):
        """Constructor for Dataloader"""
        self.dataset = dataset
        self.map_cfg = map_cfg
        self.batch_cfg = batch_cfg
        self.config = config
        self.map_ops = None
        self.per_batch_map = None

        if self.map_cfg is not None and self.map_cfg.operations is not None:
            self.map_ops = build_transforms(self.map_cfg.operations)

        if self.batch_cfg is not None and self.batch_cfg.per_batch_map is not None:
            self.per_batch_map = build_transforms(self.batch_cfg.per_batch_map)

    def __call__(self):
        """Generate mindspore dataset object."""
        if self.config.thread_num >= 0:
            cv2.setNumThreads(self.config.thread_num)
        if 'prefetch_size' in self.config:
            de.config.set_prefetch_size(self.config.prefetch_size)

        ds = self.dataset
        if self.map_ops is not None:
            self.map_cfg.pop('operations')
            ds = ds.map(operations=self.data_augment, **self.map_cfg)

        if self.per_batch_map is not None:
            self.batch_cfg.pop('per_batch_map')
            ds = ds.batch(per_batch_map=self.per_batch_map, **self.batch_cfg)
        else:
            ds = ds.batch(**self.batch_cfg)

        return ds

    def data_augment(self, *args):
        """Data augmentation function."""
        result = args
        for op in self.map_ops:
            result = op(result)
        return result


def build_dataloader(cfg, is_training=True, is_infering=False):
    """
    Build dataset loading class.

    Args:
        cfg(dict): configs
        is_training: training flag

    Returns:
        DataLoader object.
    """
    if is_training:
        config = cfg.train
    else:
        config = cfg.eval

    if is_infering:
        config = cfg.infer

    # any data source convert to mindrecord
    if config.mindrecord:
        x2mindrecord = build_dataset(config.mindrecord)
        x2mindrecord()
    # used for custom dataset source
    if 'source' in config.dataset and config.dataset.source.type is not None:
        config.dataset.source = build_dataset(config.dataset.source)

    # init custom dataset sampler, sampler use for custom dataset source
    if config.dataset.source is not None and config.dataset.sampler is not None:
        config.dataset.sampler.dataset_size = len(config.dataset.source)
        config.dataset.sampler = build_dataset_sampler(config.dataset.sampler)

    # generate dataset objcet
    ds = build_dataset(config.dataset)

    # init dataset loader
    dataset_loader = DataLoader(ds, config.map, config.batch, cfg)
    return dataset_loader
