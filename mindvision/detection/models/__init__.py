# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""models module, import every backbone, neck, head to register classes"""
from models.backbone.darknet import CspDarkNet, DarkNet
from models.backbone.resnet50 import ResNetFea

from models.detector.faster_rcnn import FasterRCNN
from models.detector.yolo import YOLOv4, YOLOv3

from models.head.roi_head import StandardRoIHead
from models.head.rpn import RPNHead
from models.head.yolo_head import YOLOv4Head, YOLOv3Head

from models.losses.mse_loss import MseLoss

from models.neck.fpn import FPN
from models.neck.yolo_neck import YOLOv4Neck, YOLOv3Neck

from models.meta_arch.train_wrapper import TrainingWrapper
