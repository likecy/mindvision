# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
 Produce the loss
"""

import sys

import mindvision.classification.models.loss as LOSS


def create_loss(config):
    mod_loss = sys.modules[LOSS.__name__]

    loss = getattr(mod_loss, config.LOSS.function, None)
    assert loss, "No loss named {}.".format(config.LOSS.function)

    loss_param = config.LOSS.params
    loss = loss(**loss_param) if loss_param else loss()

    return loss
