# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
update
"""
from .lenet import Lenet5
from .mobilenet_v2 import MobileNetV2
from .resnet import resnet18, resnet50, resnet101, se_resnet50
from .shufflenet_v2 import ShuffleNetV2
from .mobilenet_v3 import MobileNetV3

__all__ = [
    'Lenet5',
    'MobileNetV2',
    'resnet18',
    'resnet50',
    'resnet101',
    'se_resnet50',
    'ShuffleNetV2',
    'MobileNetV3'
]
