# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
 Produce the optimizer
"""

import mindspore.nn as nn
from .learning_rate import get_lr


def create_optimizer(param, config, batches_per_epoch):
    """
    create optimizer for model
    """
    # get learning rate
    config_learning_rate = config.LEARNING_RATE.params

    lr = get_lr(lr_init=config_learning_rate.lr_init,
                lr_end=config_learning_rate.lr_end,
                lr_max=config_learning_rate.lr_max,
                warmup_epochs=config_learning_rate.warmup_epochs,
                total_epochs=config.epochs,
                steps_per_epoch=batches_per_epoch,
                lr_decay_mode=config_learning_rate.decay_method)

    # define optimization
    config_optimizer = config.OPTIMIZER
    if config_optimizer.function == "Momentum":
        optimizer = nn.Momentum(param, lr,
                                config_optimizer.params.momentum)
        return optimizer
    return None
