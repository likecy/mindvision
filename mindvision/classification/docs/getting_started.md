# 快速入门

此教程主要针对初级用户，内容主要包括利用MindVision的classification进行网络训练。

## 基础知识

图像分类顾名思义就是一个模式分类问题，是计算机视觉中最基础的任务，它的目标是将不同的图像，划分到不同的类别：

- train/val/test dataset分别代表模型的训练集、验证集和测试集

    - 训练集（train dataset）：用来训练模型，使模型能够识别不同类型的特征
    - 验证集（val dataset）：训练过程中的测试集，方便训练过程中查看模型训练程度
    - 测试集（test dataset）：训练模型结束后，用于评价模型结果的测试集

- 迭代轮数（epoch）

  模型训练迭代的总轮数，模型对训练集全部样本过一遍即为一个epoch。
  当测试错误率和训练错误率相差较小时，可认为当前迭代轮数合适；
  当测试错误率先变小后变大时，则说明迭代轮数过大，需要减小迭代轮数，否则容易出现过拟合。

- 损失函数（Loss Function）

  训练过程中，衡量模型输出（预测值）与真实值之间的差异

- 准确率（Acc）

  表示预测正确的样本数占总数据的比例

## 环境安装与配置

下载MindVision并进入文件夹

```shell
git clone https://gitee.com/mindspore/mindvision.git
cd mindvision
```

安装

```python
python setup.py install
```

## 数据的准备与处理

进入`mindvision/classification`目录。

```bash
cd mindvision/classification
```

下载并解压数据集.

我们示例中用到的MNIST数据集是由10类28∗28的灰度图片组成，训练数据集包含60000张图片，测试数据集包含10000张图片。

你可以从MNIST数据集下载页面下载，并按下方目录结构放置，或直接运行如下命令完成下载和放置：

```shell
!mkdir -p ./datasets/MNIST_Data/train ./datasets/MNIST_Data/test
!wget -NP ./datasets/MNIST_Data/train https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/train-labels-idx1-ubyte
!wget -NP ./datasets/MNIST_Data/train https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/train-images-idx3-ubyte
!wget -NP ./datasets/MNIST_Data/test https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/t10k-labels-idx1-ubyte
!wget -NP ./datasets/MNIST_Data/test https://mindspore-website.obs.myhuaweicloud.com/notebook/datasets/mnist/t10k-images-idx3-ubyte
!tree ./datasets/MNIST_Data
```

输出

```text
./datasets/MNIST_Data
├── test
│   ├── t10k-images-idx3-ubyte
│   └── t10k-labels-idx1-ubyte
└── train
    ├── train-images-idx3-ubyte
    └── train-labels-idx1-ubyte

2 directories, 4 files
```

没有安装`wget`命令或者windows中下载的话，需要将地址拷贝到浏览器中下载，并进行解压。

修改yaml文件中的路径

yaml文件中有多个参数配置，其中`data_dir`是数据集路径，将本地数据集路径填入并保存。

```text
TRAIN:
    batch_size: 32
    num_workers: 1
    dataset_name: "mnist"
    data_dir: "yourpath"
    shuffle: "True"
    shuffle_seed: 0
    transforms: None
    loss: "SoftmaxCrossEntropyWithLogits"

VALID:
    batch_size: 32
    num_workers: 1
    dataset_name: "mnist"
    data_dir: "yourpath"
    shuffle: "True"
    shuffle_seed: 0
    transforms: None
```

返回``mindvision/classification``根目录

```shell
# linux or mac
cd mindvision/classification
```

## 模型训练

```shell
python tools/train.py -c configs/lenet/lenet.yaml
```

- `-c` 参数是指定训练的配置文件路径，训练的具体超参数可查看`yaml`文件
- `yaml`文件中`epochs`参数设置为20，说明对整个数据集进行20个epoch迭代

## 模型验证

```shell
python tools/eval.py -c configs/lenet/lenet.yaml
```

- `-c` 参数是指定训练的配置文件路径，训练的具体超参数可查看`yaml`文件

## 模型推理

```shell
python tools/eval.py -c configs/lenet/lenet.yaml
```

- `-c` 参数是指定训练的配置文件路径，训练的具体超参数可查看`yaml`文件

## 模型导出

```shell
python tools/eval.py -c configs/lenet/lenet.yaml
```

- `-c` 参数是指定训练的配置文件路径，训练的具体超参数可查看`yaml`文件
