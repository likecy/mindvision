# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""test load torch parameter to mindspore model"""

import math
import torch
import mindspore
from mindspore import context
from mindspore.train import Model
from mindspore.ops import operation as ops
from mindvision.classification.utils import parse_config
from mindvision.classification.models.build_train import build_model
from mindvision.classification.utils import torch_to_ms


def test_torch_to_ms():
    # load mobilenet_v2 torch pretrained model
    torch_model = torch.hub.load("pytorch/vision:v0.9.0", 'mobolenet_v2', pretrained=True)
    torch_model.eval()

    context.set_context(mode=context.GRAPH_MODE,
                        device_target="GPU",
                        save_graphs=False)

    # build mobilenetv2 mindspore model
    config = parse_config("path to mobilenetv2 yaml")
    ms_network = build_model(config)
    torch_to_ms(ms_network, torch_model)

    # init the whole Model
    ms_model = Model(ms_network)

    # init input
    shape = (1, 3, 224, 224)
    torch_input = torch.ones(shape)
    if torch.cuda.is_available():
        torch_input = torch_input.to('cuda')
        torch_model.to('cuda')

    ones = ops.Ones()
    ms_input = ones(shape, mindspore.float32)

    # eval
    with torch.no_grad():
        torch_output = torch_model(torch_input)

    ms_outputs = ms_model.predict(ms_input)

    assert math.isclose(list(ms_outputs.asnumpy())[0][0], list(torch_output.cpu().numpy())[0][0], abs_tol=0.01)
