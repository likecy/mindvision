# MobileNetV2: Inverted Residuals and Linear Bottlenecks

## Introduction

The MobileNetV2 architecture is based on an inverted residual structure where the input and output of the residual block are thin bottleneck layers opposite to traditional residual models which use expanded representations in the input an MobileNetV2 uses lightweight depthwise convolutions to filter features in the intermediate expansion layer.

[MobileNetV2: Inverted Residuals and Linear Bottlenecks](https://arxiv.org/pdf/1801.04381.pdf)

```latex
@INPROCEEDINGS{8578572,
  author={M. {Sandler} and A. {Howard} and M. {Zhu} and A. {Zhmoginov} and L. {Chen}},
  booktitle={2018 IEEE/CVF Conference on Computer Vision and Pattern Recognition},
  title={MobileNetV2: Inverted Residuals and Linear Bottlenecks},
  year={2018},
  volume={},
  number={},
  pages={4510-4520},
  doi={10.1109/CVPR.2018.00474}}
}
```

## Training setting

| Parameters                 | MobilenetV2                                                |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | Ascend 910; cpu 2.60GHz, 192cores; memory 755G; OS Euler2.8|
| uploaded Date              | 07/20/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | mobilenetv2_imagenet2012.yaml                              |
| Optimizer                  | Momentum                                                   |
| Loss Function              | SoftmaxCrossEntropyWithLogits                              |
| Backbone                   | MobileNetV2                                                |
| Neck                       | GlobalAvgPooling                                           |
| Head                       | LinearClsHead                                              |
| Batch size                 | 32                                                         |

## Eval setting

| Parameters                 | MobilenetV2                                                |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | Ascend 910; cpu 2.60GHz, 192cores; memory 755G; OS Euler2.8|
| uploaded Date              | 07/20/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | mobilenetv2_imagenet2012.yaml                              |
| Backbone                   | MobileNetV2                                                |
| Neck                       | GlobalAvgPooling                                           |
| Head                       | LinearClsHead                                              |
| Batch size                 | 32                                                         |

## Pretrained models

### MobilenetV2 ImageNet checkpoints

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| MobileNet V2_240x240          | 3.3       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |
| MobileNet V2_180x180          | 3.3       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |
| MobileNet V2_160x160          | 3.3       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |
| MobileNet V2_86x86          | 3.3       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |

### MobilenetV2 Cifar100 checkpoints

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| MobileNet V2_240x240          | 3.5       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |
| MobileNet V2_180x180         | 3.5       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |
| MobileNet V2_160x160          | 3.5       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |
| MobileNet V2_86x86         | 3.5       | 0.319    | 71.86 | 90.42 | [config]() | [model]() |

## Example

see this [link]() to learn more detail.
