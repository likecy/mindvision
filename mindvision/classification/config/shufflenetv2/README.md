# ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design

## 简介

ShuffleNetV2是基于ShuffleNetV1改进的轻量级网络，设计目的是能让网络又准确又快地运行在移动端设备。模型的运行速度不仅仅需要与模型的计算复杂度（即FLOPS）有关，还与模型的运行内存消耗和计算平台的特性有关。考虑到以上因素，ShuffleNetV2提出了一个新的运算：channel split, 采用深度可分割卷积（depthwise separable convolutions），减少了组卷积（group convolution）模块，减少网络碎片化以提高并行度，并且减少了元素级操作（element-wise operators），使ShuffleNetV2的速度和精度都达到一个较高的水平。

[ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design](https://arxiv.org/abs/1807.11164.pdf)

```latex
@inproceedings{ma2018shufflenet,
  title={Shufflenet v2: Practical guidelines for efficient cnn architecture design},
  author={Ma, Ningning and Zhang, Xiangyu and Zheng, Hai-Tao and Sun, Jian},
  booktitle={Proceedings of the European conference on computer vision (ECCV)},
  pages={116--131},
  year={2018}
}
```

## 模型结构

ShuffleNetV2总体网络架构如下：
<table>
  <tr>
      <th>网络层</th>
      <th>输出尺寸</th>
      <th>卷积核尺寸</th>
      <th>步长</th>
      <th>重复次数</th>
      <th colspan="4">输出的通道数</th>  
  </tr >
  <tr>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>0.5x</th>
      <th>1.0x</th>
      <th>1.5x</th>
      <th>2.0x</th>
  </tr>
  <tr>
      <td>Image</td>
      <td>224x224</td>
      <td></td>
      <td></td>
      <td></td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
  </tr>
  <tr>
    <td>Conv1<br>MaxPool</td>
    <td>112x112<br>56x56</td>
    <td>3x3<br>3x3</td>
    <td>2<br>2</td>
    <td>1</td>
    <td>3</td>
    <td>3</td>
    <td>3</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Stage2</td>
    <td>28x28<br>28x28</td>
    <td></td>
    <td>2<br>1</td>
    <td>1<br>3</td>
    <td>48</td>
    <td>116</td>
    <td>176</td>
    <td>244</td>
  </tr>
  <tr>
    <td>Stage3</td>
    <td>14x14<br>14x14</td>
    <td></td>
    <td>2<br>1</td>
    <td>1<br>7</td>
    <td>96</td>
    <td>232</td>
    <td>352</td>
    <td>488</td>
  </tr>
  <tr>
    <td>Stage4</td>
    <td>7x7<br>7x7</td>
    <td></td>
    <td>2<br>1</td>
    <td>1<br>3</td>
    <td>192</td>
    <td>464</td>
    <td>704</td>
    <td>976</td>
  </tr>
  <tr>
    <td>Conv5</td>
    <td>7x7</td>
    <td>1x1</td>
    <td>1</td>
    <td>1</td>
    <td>1024</td>
    <td>1024</td>
    <td>1024</td>
    <td>2048</td>
  </tr>
  <tr>
    <td>GlobalPool</td>
    <td>1x1</td>
    <td>7x7</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>FC</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>1000</td>
    <td>1000</td>
    <td>1000</td>
    <td>1000</td>
  </tr>
</table>

## 训练参数

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Optimizer                  | Momentum                                                   |
| Loss Function              | CrossEntropySmooth                                         |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## 验证参数

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## 预训练模型

### 在ImageNet上训练后的预训练文件

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| ShuffleNet V2_x0.5    |1.4        |0.041     |  |  | [config]() | [model]() |
| ShuffleNet V2_x1.0    |2.3        |0.146     | 69.65 | 88.63 | [config](https://gitee.com/mindspore/mindvision/blob/classification_shufflenetv2/mindvision/classification/config/shufflenetv2/shufflenetv2_x1_0_imagenet2012.yaml) | [model]() |
| ShuffleNet V2_x1.5    |3.5        |0.299     |  |  | [config]() | [model]() |
| ShuffleNet V2_x2.0    |7.4        |0.591     |  |  | [config]() | [model]() |

## 样例

点击此链接 [link]() 以了解更多信息.
