# ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design

## Introduction

The ShufflenetV2 architecture is designed to speed up image classification computation. The speed of deep nerual network not only depend on computation complexity, i.e. FLOPS, but also depends on the other factors such as memory access cost and platform characterics. Taking these factors into account, this work proposes practical guidelines for efficient network design. Based on ShuffleNetV1, ShuffleNetV2 applies "channel split" to reduce the group convolution and element-wise operators.

[ShuffleNet V2: Practical Guidelines for Efficient CNN Architecture Design](https://arxiv.org/abs/1807.11164.pdf)

```latex
@inproceedings{ma2018shufflenet,
  title={Shufflenet v2: Practical guidelines for efficient cnn architecture design},
  author={Ma, Ningning and Zhang, Xiangyu and Zheng, Hai-Tao and Sun, Jian},
  booktitle={Proceedings of the European conference on computer vision (ECCV)},
  pages={116--131},
  year={2018}
}
```

## Model architecture

The overall network architecture of ShuffleNetV2 is show below:
<table>
  <tr>
      <th>Layer</th>
      <th>Output size</th>
      <th>Kernel size</th>
      <th>Stride</th>
      <th>Repeat</th>
      <th colspan="4">Output channels</th>  
  </tr >
  <tr>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>0.5x</th>
      <th>1.0x</th>
      <th>1.5x</th>
      <th>2.0x</th>
  </tr>
  <tr>
      <td>Image</td>
      <td>224x224</td>
      <td></td>
      <td></td>
      <td></td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
      <td>3</td>
  </tr>
  <tr>
    <td>Conv1<br>MaxPool</td>
    <td>112x112<br>56x56</td>
    <td>3x3<br>3x3</td>
    <td>2<br>2</td>
    <td>1</td>
    <td>3</td>
    <td>3</td>
    <td>3</td>
    <td>3</td>
  </tr>
  <tr>
    <td>Stage2</td>
    <td>28x28<br>28x28</td>
    <td></td>
    <td>2<br>1</td>
    <td>1<br>3</td>
    <td>48</td>
    <td>116</td>
    <td>176</td>
    <td>244</td>
  </tr>
  <tr>
    <td>Stage3</td>
    <td>14x14<br>14x14</td>
    <td></td>
    <td>2<br>1</td>
    <td>1<br>7</td>
    <td>96</td>
    <td>232</td>
    <td>352</td>
    <td>488</td>
  </tr>
  <tr>
    <td>Stage4</td>
    <td>7x7<br>7x7</td>
    <td></td>
    <td>2<br>1</td>
    <td>1<br>3</td>
    <td>192</td>
    <td>464</td>
    <td>704</td>
    <td>976</td>
  </tr>
  <tr>
    <td>Conv5</td>
    <td>7x7</td>
    <td>1x1</td>
    <td>1</td>
    <td>1</td>
    <td>1024</td>
    <td>1024</td>
    <td>1024</td>
    <td>2048</td>
  </tr>
  <tr>
    <td>GlobalPool</td>
    <td>1x1</td>
    <td>7x7</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td>FC</td>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    <td>1000</td>
    <td>1000</td>
    <td>1000</td>
    <td>1000</td>
  </tr>
</table>

## Training setting

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Optimizer                  | Momentum                                                   |
| Loss Function              | CrossEntropySmooth                                         |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## Eval setting

| Parameters                 | ShuffleNetV2                                               |
| -------------------------- | ---------------------------------------------------------- |
| Model Version              | V2                                                         |
| Resource                   | NV GTX2080Ti; memory 11G; OS Ubuntu18.04                   |
| uploaded Date              | 07/23/2021                                                 |
| MindSpore Version          | 1.2.0                                                      |
| Training Parameters        | shufflenetv2_x1_0_imagenet2012.yaml                        |
| Backbone                   | ShuffleNetV2                                               |
| Neck                       | AvgPooling                                                 |
| Head                       | DenseHead                                                  |
| Batch size                 | 256                                                        |

## Pretrained models

### ShuffleNetV2 ImageNet checkpoints

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| ShuffleNet V2_x0.5    |1.4        |0.041     |  |  | [config]() | [model]() |
| ShuffleNet V2_x1.0    |2.3        |0.146     | 69.65 | 88.63 | [config](https://gitee.com/mindspore/mindvision/blob/classification_shufflenetv2/mindvision/classification/config/shufflenetv2/shufflenetv2_x1_0_imagenet2012.yaml) | [model]() |
| ShuffleNet V2_x1.5    |3.5        |0.299     |  |  | [config]() | [model]() |
| ShuffleNet V2_x2.0    |7.4        |0.591     |  |  | [config]() | [model]() |

## Example

see this [link]() to learn more detail.
