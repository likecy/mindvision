# Searching for MobileNetV3

## 简介

MobileNetV3设计目的是能让网络又准确又快地运行在移动端设备。结合硬件感知神经网络架构搜索（NAS）和NetAdapt算法，MobileNetV3重新设计了耗时的层，使用h-wish替代ReLU6，扩展层使用的滤波器数量优化（使用NetAdapt算法获得最佳数量），瓶颈层输出的通道数量优化（使用NetAdapt算法获得最佳数量），Squeeze-and-excitation模块（SE）将通道数仅缩减了3或4倍，对于SE模块，不再使用sigmoid，而是采用ReLU6(x+3)/6作为近似，这些改进使MobileNetV3的速度和精度都达到一个较高的水平。

[Searching for MobileNetV3](https://arxiv.org/abs/1905.02244.pdf)

```latex
@inproceedings{Howard2019Mobilenetv3,
  author    = {Andrew Howard and
               Ruoming Pang and
               Hartwig Adam and
               Quoc V. Le and
               Mark Sandler and
               Bo Chen and
               Weijun Wang and
               Liang{-}Chieh Chen and
               Mingxing Tan and
               Grace Chu and
               Vijay Vasudevan and
               Yukun Zhu},
  title     = {Searching for MobileNetV3},
  booktitle = {2019 {IEEE/CVF} International Conference on Computer Vision, {ICCV}
               2019, Seoul, Korea (South), October 27 - November 2, 2019},
  pages     = {1314--1324},
  publisher = {{IEEE}},
  year      = {2019},
}
```

## 模型结构

MobileNetV3总体网络架构如下：

large:

| Input | Operator | exp size | #out | SE | NL | stride|
|:--------------:|:----------------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| 224^2 x 3  | conv2d       | -   | 16  | - | HS | 2 |
| 112^2 x 16 | bneck 3x3    | 16  | 16  | - | RE | 1 |
| 112^2 x 16 | bneck 3x3    | 64  | 24  | - | RE | 2 |
| 56^2 x 24  | bneck 3x3    | 72  | 24  | - | RE | 1 |
| 56^2 x 24  | bneck 5x5    | 72  | 40  | X | RE | 2 |
| 28^2 x 40  | bneck 5x5    | 120 | 40  | X | RE | 1 |
| 28^2 x 40  | bneck 5x5    | 120 | 40  | X | RE | 1 |
| 28^2 x 40  | bneck 3x3    | 240 | 80  | - | HS | 2 |
| 14^2 x 80  | bneck 3x3    | 200 | 80  | - | HS | 1 |
| 14^2 x 80  | bneck 3x3    | 184 | 80  | - | HS | 1 |
| 14^2 x 80  | bneck 3x3    | 184 | 80  | - | HS | 1 |
| 14^2 x 80  | bneck 3x3    | 480 | 112 | X | HS | 1 |
| 14^2 x 112 | bneck 3x3    | 672 | 112 | X | HS | 1 |
| 14^2 x 112 | bneck 5x5    | 672 | 160 | X | HS | 2 |
| 7^2 x 160  | bneck 5x5    | 960 | 160 | X | HS | 1 |
| 7^2 x 160  | bneck 5x5    | 960 | 160 | X | HS | 1 |
| 7^2 x 160  | conv2d 1x1   | -   | 960 | - | HS | 1 |
| 7^2 x 960  | pool 7x7     | -   | -   | - | -  | 1 |
| 1^2 x 960  | conv2d 1x1 NBN | - | 1280 | - | HS | 1 |
| 1^2 x 1280 | conv2d 1x1 NBN | - | k  | - | -  | 1 |

small:

| Input | Operator | exp size | #out | SE | NL | stride|
|:--------------:|:----------------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| 224^2 x 3  | conv2d 3x3   | -   | 16  | - | HS | 2 |
| 112^2 x 16 | bneck 3x3    | 16  | 16  | X | RE | 2 |
| 56^2 x 16  | bneck 3x3    | 72  | 24  | - | RE | 2 |
| 28^2 x 24  | bneck 3x3    | 88  | 24  | - | RE | 1 |
| 28^2 x 24  | bneck 5x5    | 96  | 40  | X | HS | 2 |
| 14^2 x 40  | bneck 5x5    | 240 | 40  | X | HS | 1 |
| 14^2 x 40  | bneck 5x5    | 240 | 40  | X | HS | 1 |
| 14^2 x 40  | bneck 5x5    | 120 | 48  | X | HS | 1 |
| 14^2 x 48  | bneck 5x5    | 144 | 48  | X | HS | 1 |
| 14^2 x 48  | bneck 5x5    | 288 | 96  | X | HS | 2 |
| 7^2 x 96   | bneck 5x5    | 576 | 96  | X | HS | 1 |
| 7^2 x 96   | bneck 5x5    | 576 | 96  | X | HS | 1 |
| 7^2 x 96   | conv2d 1x1   | -   | 576 | X | HS | 1 |
| 7^2 x 576  | pool 7x7     | -   | -   | - | -  | 1 |
| 1^2 x 576  | conv2d 1x1 NBN | - | 1024 | - | HS | 1 |
| 1^2 x 1024 | conv2d 1x1 NBN | - | k  | - | -  | 1 |

## 训练参数

| Parameters          | MobileNetV3                             |
| ------------------- | --------------------------------------- |
| Model Version       | large                                   |
| Resource            | NV GTX3060Ti; memory 8G; OS Ubuntu18.04 |
| uploaded Date       | 08/10/2021                              |
| MindSpore Version   | 1.3.0                                   |
| Training Parameters | mobilenetv3_large_imagenet2012.yaml     |
| Optimizer           | Momentum                                |
| Loss Function       | CrossEntropySmooth                      |
| Backbone            | MobileNetV3                             |
| Neck                | AvgPooling                              |
| Head                | LinearClsMobilenetv3Head                |
| Batch size          | 256                                     |

## 验证参数

| Parameters          | MobileNetV3                             |
| ------------------- | --------------------------------------- |
| Model Version       | large                                   |
| Resource            | NV GTX3060Ti; memory 8G; OS Ubuntu18.04 |
| uploaded Date       | 08/10/2021                              |
| MindSpore Version   | 1.3.0                                   |
| Training Parameters | mobilenetv3_large_imagenet2012.yaml     |
| Backbone            | MobileNetV3                             |
| Neck                | AvgPooling                              |
| Head                | LinearClsMobilenetv3Head                |
| Batch size          | 256                                     |

## 预训练模型

### 在ImageNet上训练后的预训练文件

|         Model         | Params(M) | Flops(G) | Top-1 (%) | Top-5 (%) | Config | Download |
|:---------------------:|:---------:|:--------:|:---------:|:---------:|:---------:|:--------:|
| MobileNetV3 large | 5.4 | 0.227 | 74.43 | 91.75 | [config](https://gitee.com/mindspore/mindvision/tree/master/mindvision/classification/config/mobilenetv3/<br/>mobilenetv3_large_imagenet2012.yaml) | [model]() |
| MobileNetV3 small | 2.5 | 0.059 | 65.22 | 86.17 | [config](https://gitee.com/mindspore/mindvision/tree/master/mindvision/classification/config/mobilenetv3/<br/>mobilenetv3_small_imagenet2012.yaml) | [model]() |

## 样例

点击此链接 [link]() 以了解更多信息.

