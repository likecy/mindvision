# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""
Image Processing
"""
import os
import numpy as np
import cv2


def preprocess(img, args):
    resize_op = ResizeImage(resize_short=args.INFER.image_shape)
    img = resize_op(img)
    return img


def postprocess(batch_outputs, topk=5, multilabel=False):
    """
    image postprocess
    """
    batch_results = []
    for probs in batch_outputs:
        if multilabel:
            index = np.where(probs >= 0.5)[0].astype('int32')
        else:
            index = probs.argsort(axis=0)[-topk:][::-1].astype("int32")
        clas_id_list = []
        score_list = []
        for i in index:
            clas_id_list.append(i.item())
            score_list.append(probs[i].item())
        batch_results.append({"clas_ids": clas_id_list, "scores": score_list})
    return batch_results


def get_image_list(img_file):
    """
    get images list from directory
    """
    imgs_lists = []
    if img_file is None or not os.path.exists(img_file):
        raise Exception("not found any img file in {}".format(img_file))

    img_end = ['jpg', 'png', 'jpeg', 'JPEG', 'JPG', 'bmp']
    if os.path.isfile(img_file) and img_file.split('.')[-1] in img_end:
        imgs_lists.append(img_file)
    elif os.path.isdir(img_file):
        for single_file in os.listdir(img_file):
            if single_file.split('.')[-1] in img_end:
                imgs_lists.append(os.path.join(img_file, single_file))
    if len(imgs_lists) == 0: # pylint: disable=len-as-condition
        raise Exception("not found any img file in {}".format(img_file))
    return imgs_lists


class ResizeImage:
    """
    resize image
    """
    def __init__(self, resize_short=None):
        self.resize_short = resize_short

    def __call__(self, img):
        resize_h = self.resize_short[1]
        resize_w = self.resize_short[2]
        return cv2.resize(img, (resize_w, resize_h))


class CropImage:
    """
    crop image
    """
    def __init__(self, size):
        if type(size) is int:  # pylint: disable=unidiomatic-typecheck
            self.size = (size, size)
        else:
            self.size = size

    def __call__(self, img):
        w, h = self.size
        img_h, img_w = img.shape[:2]
        w_start = (img_w - w) // 2
        h_start = (img_h - h) // 2

        w_end = w_start + w
        h_end = h_start + h
        return img[h_start:h_end, w_start:w_end, :]


class NormalizeImage:
    """
    normalize image
    """
    def __init__(self, scale=None, mean=None, std=None):
        self.scale = np.float32(scale if scale is not None else 1.0 / 255.0)
        mean = mean if mean is not None else [0.485, 0.456, 0.406]
        std = std if std is not None else [0.229, 0.224, 0.225]

        shape = (1, 1, 3)
        self.mean = np.array(mean).reshape(shape).astype('float32')
        self.std = np.array(std).reshape(shape).astype('float32')

    def __call__(self, img):
        return (img.astype('float32') * self.scale - self.mean) / self.std
