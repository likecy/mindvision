"""
Build transforms from config.
"""

import sys
import mindspore.dataset.vision.c_transforms as C
import mindspore.dataset.transforms.c_transforms as C2
import numpy as np

class RGBtoBGR():
    """
    Change image from RGB to BGR.

    Args:
        img: image in type RGB.

    Returns: image in type BGR.

    """
    def __init__(self):
        pass

    def __call__(self, img):
        img = img[:, :, ::-1]
        img = np.ascontiguousarray(img)
        return img


def build_trans(pre_process):
    """
    Build transform operations pre_process dict.

    Args:
        pre_process: Dict contains transform operations and their parameters.

    Returns:
        trans: List of transforms.

    """
    trans = []
    modv = sys.modules[C.__name__]
    modc = sys.modules[C2.__name__]
    modn = sys.modules[__name__]
    for op, param in pre_process.items():
        transv = getattr(modv, op, None)
        transc = getattr(modc, op, None)
        transn = getattr(modn, op, None)

        assert (transv or transc or transn), \
                "No transform named {}.".format(op)

        if transv:
            trans.append(transv(**param) if param else transv())
        elif transc:
            trans.append(transc(**param) if param else transc())
        elif transn:
            trans.append(transn(**param) if param else transn())

    return trans
