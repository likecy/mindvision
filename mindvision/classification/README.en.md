[Docs] | [中文版]

[Docs]: https://www.mindspore.cn/
[中文版]: README.md

## Introduction

MindVison Classification is an open source image classification toolbox based on MindSpore.

## Major features

- Modular Design

We decompose the classification framework into different components and one can easily construct a customized object classification framework by combining different modules.

## License

This project is released under the [Apache 2.0 license](LICENSE).

## Benchmark and model zoo

Results and models are available in the [model zoo](docs/model_zoo.md).

Supported models:

- [x] [LeNet]()
- [ ] ResNet
- [ ] ResNeXt
- [ ] SE-ResNet
- [ ] SE-ResNeXt
- [ ] RegNet
- [ ] ShuffleNetV1
- [ ] ShuffleNetV2
- [x] [MobileNetV2](config/mobilenetv2/README.en.md)
- [ ] MobileNetV3

Supported datasets:

- [x] [MNIST](http://yann.lecun.com/exdb/mnist/)
- [x] [ImageNet2012](https://image-net.org/challenges/LSVRC/index.php)

Please see [getting_dataset_started.md](docs/getting_dataset_started.md) for the basic usage of the interface of datasets.

## Getting Started

Please see [getting_started.md](docs/getting_started.md) for the basic usage of MindVision classification.

## Feedbacks and Contact

The dynamic version is still under development, if you find any issue or have an idea on new features, please don't hesitate to contact us via [Gitee Issues](https://gitee.com/mind_spore/mindspore-segmentation/issues).

## Contributing

We appreciate all contributions to improve MindSpore classification. Please refer to [CONTRIBUTING.md](./CONTRIBUTING.md) for the contributing guideline.

## Acknowledgement

MindVison classification is an open source project that welcome any contribution and feedback.
We wish that the toolbox and benchmark could serve the growing research
community by providing a flexible as well as standardized toolkit to reimplement existing methods
and develop their own new classification methods.

## Other projects in MindVision

- [Detection](): MindVision detection toolbox and benchmark.