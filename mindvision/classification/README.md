[文档] | [English Version]

[文档]: https://www.mindspore.cn/
[English Version]: README.en.md

## 简介

MindVison Classification是一个基于MindSpore的计算机视觉开源工具箱。

## 主要特性

- 模块化设计

我们将分类框架解耦成不同的模块组件，通过组合不同的模块组件，用户可以便捷的构建自定义的分类模型。

## 开源许可证

该项目采用[Apache 2.0 开源许可证](LICENSE)。

## 基准测试和模型库

测试结果和模型可以在[模型库](docs/model_zoo.md)中找到。

已支持的骨干网络：

- [x] [LeNet]()
- [ ] ResNet
- [ ] ResNeXt
- [ ] SE-ResNet
- [ ] SE-ResNeXt
- [ ] RegNet
- [ ] ShuffleNetV1
- [ ] ShuffleNetV2
- [x] [MobileNetV2](config/mobilenetv2/README.en.md)
- [ ] MobileNetV3

已支持的数据集：

- [x] [MNIST](http://yann.lecun.com/exdb/mnist/)
- [x] [ImageNet2012](https://image-net.org/challenges/LSVRC/index.php)

请参考[数据处理快速入门文档](docs/getting_dataset_started.md)学习数据处理接口的基本使用。

## 快速入门

请参考[快速入门文档](docs/getting_started.md)学习MindVision classification的基本使用。

## 反馈与联系

动态版本仍在开发中，如果您发现任何问题或对新特性有想法，请随时通过[Gitee Issues](https://gitee.com/mindspore/mindvision/issues)与我们联系。

## 贡献指南

我们为所有改进和提升MindVison classification的贡献者表示感谢。请参考[贡献指南](CONTRIBUTING.md)来了解参与项目贡献的相关指引。

## 致谢

MindVison classification是一款开源项目，我们感谢所有为项目提供贡献的贡献者，以及提供宝贵反馈的用户。我们希望这个工具箱和基准测试可以为社区提供灵活的代码工具，供用户复现已有算法并开发自己的分类模型，从而不断为开源社区提供贡献。

## MindVision的其他项目

- [Detection](): MindVision目标检测工具箱。