# MindVision

[文档] | [English Version]

[文档]: https://github.com/RUCAIBox/RecBole
[English Version]: README.en.md

## 简介

MindVision是一个基于MindSpore的计算机视觉开源工具箱。

主分支使用**MindSpore 1.2**版本。

## 安装

请参考[快速入门文档]()进行安装。

## 开源许可证

该项目采用[Apache 2.0 开源许可证](LICENSE)。

## 反馈与联系

动态版本仍在开发中，如果您发现任何问题或对新特性有想法，请随时通过[Gitee Issues](https://gitee.com/mindspore/mindvision/issues)与我们联系。

## MindVision的项目

- [Classification](mindvision/classification/README.en.md): MindVision图像分类工具箱。
- [Detection](): MindVision目标检测工具箱。

## 致谢

MindVison是一款开源项目，我们感谢所有为项目提供贡献的贡献者，以及提供宝贵反馈的用户。我们希望这个工具箱和基准测试可以为社区提供灵活的代码工具，供用户复现已有算法并开发自己的计算机视觉模型，从而不断为开源社区提供贡献。

## 贡献指南

我们为所有改进和提升MindVison的贡献者表示感谢。请参考[贡献指南](CONTRIBUTING.md)来了解参与项目贡献的相关指引。

## 引用

如果这个项目在你的研究中是有用的，请参考如下引用：

```latex
@misc{mindvsion2021,
    title={{MindVision}:MindSpore Vision Toolbox and Benchmark},
    author={MindVision Contributors},
    howpublished = {\url{https://gitee.com/mindspore/mindvision}},
    year={2021}
}
```